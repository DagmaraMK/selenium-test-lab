import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_13_HomePage_menu_Test extends SeleniumBaseTest {

    @Test
    public void goToProcessesTest() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .assertProcessesHeaderIsShown()
                .assertUrl("http://localhost:4444/Projects");
    }

    @Test
    public void testMenu() {
        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .assertUrl("http://localhost:4444/Projects")
                .assertProcessesHeaderIsShown()
                .goToCharacteristics()
                .assertCharacteristicsHeaderIsShown()
                .assertUrl("http://localhost:4444/Characteristics")
                .goToDashboards()
                .assertDemoProjectIsShown("Dashboard")
                .assertDashboardUrl("http://localhost:4444/Dashboard");
    }
}

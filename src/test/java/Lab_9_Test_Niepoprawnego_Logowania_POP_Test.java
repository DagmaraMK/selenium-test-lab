import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_9_Test_Niepoprawnego_Logowania_POP_Test extends SeleniumBaseTest {

    @Test
    public void incorrectLoginTest() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.typeEmail("test2@test.com");
        loginPage.typePassword("Test1!");
        loginPage.submitLoginWithFailure();
        loginPage.assertLoginErrorIsShown("Invalid login attempt.");
    }

    @Test
    public void incorrectLoginTestWithChaining() {
        new LoginPage(driver)
                .typeEmail("test2@test.com")
                .typePassword("Test1!")
                .submitLoginWithFailure()
                .assertLoginErrorIsShown("Invalid login attempt.");
    }
}

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Lab_6_Test_Niepoprawnego_Logowania_Password_Test {
    @Test

    public void incorrectLoginTestWrongPassword(Label emailError, String expected) {
        System.setProperty("webdriver.chrome.driver", "c:/dev/driver/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        driver.manage().window().maximize();

        driver.get("http://localhost:4444/");

        WebElement emailTxt = driver.findElement(By.cssSelector("#Email"));
        emailTxt.sendKeys("test");

        WebElement passwordTxt = driver.findElement(By.cssSelector("#Password"));
        passwordTxt.sendKeys("test111");

        WebElement loginBtn = driver.findElement(By.cssSelector("button[type=submit]"));
        loginBtn.click();

        List<WebElement> validationErrors=driver.findElements(By.cssSelector(".validation-summary-errors>ul>li"));
        Assert.assertEquals(validationErrors.get(0).getText(), "Invalid login attempt.");
        Assert.assertEquals(validationErrors.size(),  1);

            driver.quit();
        }
    }


import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_14_Menu_Test extends SeleniumBaseTest {

    @Test
    public void testMenu() {
        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .assertUrl(config.getApplicationUrl() + "Projects")
                .assertProcessesHeaderIsShown()
                .goToCharacteristics()
                .assertUrl(config.getApplicationUrl() + "Characteristics")
                .assertCharacteristicsHeaderIsShown()
                .goToDashboards()
                .assertDashboardUrl(config.getApplicationUrl())
                .assertDashboardHeaderIsShown();

    }
}
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Lab_11_Test_Niepoprawnej_Rejestracji_DataSource_Test extends SeleniumBaseTest {


    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"test.com"},
                {"@test.com"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectEmailTest(String wrongEmail) {
        new LoginPage(driver)
                .goToRegisterLink()
                .typeEmail(wrongEmail)
                .submitRegisterWithFailure()
                .assertEmailErrorIsShown("The Email field is not a valid e-mail address.");

    }
}

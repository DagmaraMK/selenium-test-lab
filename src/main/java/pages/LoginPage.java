package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;


public class LoginPage {

    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(css = "#Password")
    private WebElement passwordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;

    @FindBy(css = "#Email-error")
    public WebElement emailError;

    @FindBy(css = "a[href^='/Account']")
    private WebElement registerLink;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> loginErrors;

    @FindBy(css = "")
    private WebElement userProfile;

    @FindBy(css = "a[href*=Logout]")
    private WebElement logoutLink;

    @FindBy(css = ".profile_info>h2")
    public WebElement welcomeElm;

    @FindBy(css = ".menu-home")
    private WebElement homeNav;

    @FindBy(linkText = "Dashboard")
    private WebElement dashboardMenu;

    @FindBy(css = ".menu-workspace")
    private WebElement workspaceNav;

    @FindBy(css = "a[href$=Projects]")
    private WebElement processesMenu;

    @FindBy(css = "a[href$=Characteristics]")
    private WebElement characteristicsMenu;

    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public HomePage submitLogin() {
        loginBtn.click();
        return new HomePage(driver);
    }

    public LoginPage submitLoginWithFailure() {
        loginBtn.click();
        return this;
    }

    public CreateAccountPage goToRegisterLink() {
        registerLink.click();
        return new CreateAccountPage(driver);
    }

    public LoginPage assertLoginErrorIsShown(String expError) {
        boolean isErrorShown = loginErrors.stream().anyMatch(error -> error.getText().equals(expError));
        Assert.assertTrue(loginErrors.get(0).isDisplayed(), "No errors displayed");
        Assert.assertTrue(isErrorShown, "Error: '" + expError + "' is not shown");

        return this;
    }

    public LoginPage assertEmailErrorIsShown(String expError) {
        Assert.assertTrue(emailError.isDisplayed(), "Email error is not shown");
        Assert.assertEquals(emailError.getText(), expError, "Error: '" + expError + "' is not shown");

        return this;
    }
}



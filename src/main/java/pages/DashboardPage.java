package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DashboardPage extends HomePage {

    @FindBy(css = ".x_title>h2")
    private WebElement demoProjectHeader;


    public DashboardPage(WebDriver driver) {
        super(driver);
    }


    public DashboardPage assertDemoProjectIsShown(String expError) {
        Assert.assertTrue(demoProjectHeader.isDisplayed());
        Assert.assertEquals(demoProjectHeader.getText(), "Dashboard");
        return this;
    }


    @FindBy(xpath = "//h2[contains(text(),'DEMO PROJECT')]")
    public WebElement dashboardHead;

    public DashboardPage assertDashboardHeaderIsShown() {
        Assert.assertTrue(dashboardHead.isDisplayed());
        Assert.assertEquals(dashboardHead.getText(), "DEMO PROJECT");
        return this;
    }

    public DashboardPage assertDashboardUrl(String expUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), expUrl);
        return this;
    }
}

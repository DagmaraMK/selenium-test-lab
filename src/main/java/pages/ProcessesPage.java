package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class ProcessesPage extends HomePage {

    public ProcessesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".x_title>h2")
    WebElement processesTxt;

    public ProcessesPage assertProcessesHeaderIsShown() {
        Assert.assertTrue(processesTxt.isDisplayed());
        Assert.assertEquals(processesTxt.getText(), "Processes");
        return this;
    }

    public ProcessesPage assertUrl(String expUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), expUrl);
        return this;
    }
}
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class CharacteristicsPage extends HomePage {

    public CharacteristicsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".x_title>h2")
    WebElement characteristicsTxt;

    public CharacteristicsPage assertCharacteristicsHeaderIsShown() {
        Assert.assertTrue(characteristicsTxt.isDisplayed());
        Assert.assertEquals(characteristicsTxt.getText(), "Characteristics");
        return this;
    }

    public CharacteristicsPage assertUrl(String expUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), expUrl);
        return this;
    }
    }


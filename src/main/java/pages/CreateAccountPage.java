package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CreateAccountPage {

    private WebDriver driver;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(css = "#Password")
    private WebElement passwordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement registerLnk;

    @FindBy(css = "#Email-error")
    public WebElement emailError;

    public CreateAccountPage typeEmail(String email) {
        emailTxt.sendKeys(email);
        return this;
    }

    public CreateAccountPage submitRegisterWithFailure() {
        registerLnk.click();
        return this;
    }

    public CreateAccountPage assertEmailErrorIsShown(String expError) {
        Assert.assertTrue(emailError.isDisplayed(), "Email error is not shown");
        Assert.assertEquals(emailError.getText(), expError, "Error: '" + expError + "' is not shown");

        return this;
    }
}








